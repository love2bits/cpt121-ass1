import java.util.*;
/*
Author: Miss Amy Dee DEMPSTER
E-mail: s3572869@student.rmit.edu.au
Unit:   CPT121
░█▀█░█▀▀░█▀▀░▀█▀░█▀▀░█▀█░█▄█░█▀▀░█▀█░▀█▀░░░▀█░
░█▀█░▀▀█░▀▀█░░█░░█░█░█░█░█░█░█▀▀░█░█░░█░░░░░█░
░▀░▀░▀▀▀░▀▀▀░▀▀▀░▀▀▀░▀░▀░▀░▀░▀▀▀░▀░▀░░▀░░░░▀▀▀
*/ 
public class TaxiCalculator { 
    // (Stage three)
    public static void main(String[] args) {
        
        // Establish some rates:
        final String[] RATE_BANDS = {
                "Wee hours", 
                "Day", 
                "Evening", 
                "Night"
            };
        final double[][] SCHEDULE = {
                // Start hour, flagfall, rate per km:
                {00, 6.00, 3.00}, // (Wee hours rate band)
                {05, 4.00, 2.00}, // (Daytime rate band)
                {17, 5.00, 2.50}, // (Evening rate band)
                {22, 6.00, 3.00}, // (Night rate band)
            }; 
        
        // Surcharges:
        final float PEAK_RATE = 3.50f; 
        final byte  PEAK_FLAGFALL = 8; 
        
        final byte  BOOKING_SURCHARGE = 5;
        final byte  AIRPORT_SURCHARGE = 15;
        final byte  MAXI_SURCHARGE = 14;
        final byte  DROPOFF_SURCHARGE = 5;
        
        
        // Initialise variables for user input and defaults:
        boolean isBooked  = false;
        boolean isMaxi    = false;
        String yesNo = null;
        String day = null;
        String rawTime = null;
        byte[] time = {-1, -1}; 
        String pickUp = null;
        byte passengers = 0;
        // Maximum for maxi taxis:
        final byte MAX_PASSENGERS = 11;
        // Triggers need for maxi taxi:
        final byte MAXI_THRESHOLD = 5; 
        final byte MAX_DROPOFFS = 4;
        List<String> dropOff = new ArrayList<String>(MAX_DROPOFFS);
        float distance = 0; 
        double flagfall = 0;
        double rate = 0; 
        
        System.out.println("*** Stage 3 Taxi Fare Calculator ***\n");
        
        
        // Get all booking details:
        // (More validation could be added to the while loops)
        Scanner taxiInput = new Scanner(System.in);
        while (day == null || day.equals("")) {
            System.out.print("Enter day: ");
            day = taxiInput.nextLine();
        }
        while ((time[0] < 0) || (time[0] > 24)) {
            System.out.print("Enter time in '24:00' format: ");
            rawTime = taxiInput.nextLine();
            if (rawTime.contains(":")) {
                time[0] = (byte) Integer.parseInt(rawTime.split(":")[0]);
                time[1] = (byte) Integer.parseInt(rawTime.split(":")[1]);
            }
        }
        while (pickUp == null || pickUp.equals("")) {
            System.out.print("Enter pick-up point: ");
            pickUp = taxiInput.nextLine();
        }
        boolean isAirport = (pickUp.toLowerCase().contains("airport"));

        while (dropOff.size() == 0) {
            System.out.print("Enter drop-off point: ");
            dropOff.add(taxiInput.nextLine());
        }
        while (passengers <= 0 || passengers > MAX_PASSENGERS) {
            System.out.print("Enter no. of passengers: ");
            passengers = taxiInput.nextByte();
            if (passengers > MAX_PASSENGERS)
                System.out.print("The maximum is: " + MAX_PASSENGERS + "\n");
            else 
                isMaxi = (passengers >= MAXI_THRESHOLD);
        }
        while (distance <= 0.0f) {
            System.out.print("Enter estimated trip distance in km: ");
            distance = taxiInput.nextFloat();
        }
        while (yesNo == null) {
            System.out.print("Fare pre-booked? (Y/N): ");
            // Scanner bug:
            taxiInput.nextLine(); 
            yesNo = taxiInput.nextLine().toUpperCase(); 
            isBooked = (yesNo.startsWith("Y"));
        }
        byte possibleDropOffs = (byte) Math.min(MAX_DROPOFFS, passengers);
        while (dropOff.size() < possibleDropOffs) {
            System.out.print("\nAdd additional drop-off point (Y/N): ");
            yesNo = taxiInput.nextLine().toUpperCase();
            if (yesNo.startsWith("Y")) {
                System.out.print("Enter drop-off location: ");
                dropOff.add(taxiInput.nextLine());
                System.out.print("Enter additional distance in km: ");
                distance += taxiInput.nextFloat();
                // Clear buffer:
                taxiInput.nextLine(); 
            }
            else if (yesNo.startsWith("N"))
                // Stop asking, if the answer is "N":
                break; 
            else
            	System.out.println("Please answer 'yes' or 'no'.");
        }
        byte extraDropOffs = (byte) (dropOff.size()-1);
        boolean hasExtraDropOffs = (extraDropOffs > 0);
        taxiInput.close();
        
        // Parse days of the week:
        byte dayNumber;
        final String[] DAYS = {"Sunday", "Monday", "Tuesday",
                "Wednesday", "Thursday", "Friday", "Saturday"};
        for (dayNumber = 0; dayNumber < DAYS.length; dayNumber++) {
            // Check first couple of characters of the strings to identify day name:
            if ( day.substring(0,2).equalsIgnoreCase(DAYS[dayNumber]
                    .substring(0,2)) ) {
                day = DAYS[dayNumber]; 
                break;
            }
        }
        
        
        // Print all the calculated details:
        System.out.println("\n*** Taxi Fare Details ***\n");
        
        System.out.printf("Day:                  %s%n", day);
        System.out.printf("Pick-up point:        %s%n", pickUp);
        System.out.printf("Drop-off point:       %s%n", dropOff.get(0));
        System.out.printf("Pick-up time:         %d:%02d%n", time[0], time[1]);
        System.out.printf("No. of passengers:    %d%n", passengers);
        // We could just print this with System.out.println(dropOff);,
        // but this looks much nicer:
        if (hasExtraDropOffs) {
            System.out.print("Extra drop-off at:    " + dropOff.get(1));
            for (byte i = 2; i <= extraDropOffs; i++) {
                if (i < extraDropOffs)
                    System.out.print(", " + dropOff.get(i));
                else 
                    System.out.print(" and " + dropOff.get(i));
            }
            System.out.print("\n");
        }
        System.out.printf("Estimated distance:   %,.1f km%n%n", distance);

        // Fetch flagfall & km rate from schedule of fees (working backwards):
        byte bandNumber;
        for (bandNumber = (byte) (SCHEDULE.length-1); bandNumber >= 0; bandNumber-- ) {
            if (time[0] >= SCHEDULE[bandNumber][0]) {
                flagfall = SCHEDULE[bandNumber][1];
                rate = SCHEDULE[bandNumber][2];
                break;
            }
        }
        
        // Fix issue of night-times spilling into the small hours of next morning:
        if (bandNumber == 0) {
            // Change small hours to night time:
            bandNumber = 3; 
            System.out.printf("Wee hours adjustment: Early morning of " + day );
            // Change to the day before:
            dayNumber--; 
            if (dayNumber < 0)
                // Make Saturday the day before Sunday:
                dayNumber = 6; 
            day = DAYS[dayNumber];
            System.out.printf(" counted as " + day + " night.%n");
        }
        
        // Inform of rate band, & begin spitting out calculations:
        System.out.printf("Rate band:            %-2s", RATE_BANDS[bandNumber]);
        
        // Apply weekend peak modifications to rate and flagfall:
        if ( (bandNumber == 3) && (dayNumber >= 5) ) { 
            flagfall = PEAK_FLAGFALL;
            rate = PEAK_RATE;
            System.out.printf(" (weekend peak)");
        }
        
        // Work out the base fare now:
        double fare = (distance * rate);
        System.out.printf("%nRate charged:         $%,6.2f per km", 
                (float) rate);
        System.out.printf("%nBase fare:            $%,6.2f%n", 
                (float) fare);
        
        
        // Apply flagfall and misc surcharges:
        fare += flagfall;
        System.out.printf("Flagfall:             $%,6.2f%n", 
                (float) flagfall);
        if (isBooked) {
            fare += BOOKING_SURCHARGE;
            System.out.printf("Booking surcharge:    $%,6.2f%n", 
                    (float) BOOKING_SURCHARGE);
        }
        if (isAirport) {
            fare += AIRPORT_SURCHARGE;
            System.out.printf("Airport surcharge:    $%,6.2f%n", 
                    (float) AIRPORT_SURCHARGE);
        }
        if (isMaxi) {
            fare += MAXI_SURCHARGE; 
            System.out.printf("Maxi-taxi surcharge:  $%,6.2f%n", 
                    (float) MAXI_SURCHARGE);
        }
        if (hasExtraDropOffs) {
            fare += (DROPOFF_SURCHARGE * extraDropOffs);
            System.out.printf("Each extra drop-off:  $%,6.2f ($%.2f × %d)%n",
                    (float) (DROPOFF_SURCHARGE * extraDropOffs),
                    (float) DROPOFF_SURCHARGE,
                    (byte) extraDropOffs);
        }
        
        // And the final damage:
        System.out.printf("Total fare charged:   $%,6.2f%n", fare);        
    }
    // END OF STAGE 3
}
